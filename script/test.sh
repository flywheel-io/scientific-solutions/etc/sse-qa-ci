#!/usr/bin/env bash
# gearexchangeupdate.sh -
set -eEuo pipefail
USAGE="Usage: $0

- TBD
"
DIR=$(cd "${0%/*}/.." && pwd) # set DIR as to the parent directory of the script
. "/qa-ci/scripts/utils.sh" # source the utils.sh script
. "/qa-ci/scripts/jobs.sh" # source the jobs.sh script
test -z "${DEBUG:-}" || set -x # set -x if DEBUG is set


main() {
    echo "$*" | grep -Eqvw "help|--help|-h" || { echo "$USAGE"; exit; }
    git_login
    GEAR_NAME=$(jq -r .name manifest.json)
     # TODO maybe use the commit tag env var instead
    CURRENT_GEAR_VERSION=$(jq -r .version manifest.json | sed 's/_.*//')
    GEAR_IMG=$(jq -r '.custom."gear-builder".image' manifest.json)
    IMG_NAMESAPCE=$(jq -r '.custom."gear-builder".image' manifest.json | sed 's#/.*##')
    GEAR_EXCHANGE_REPO="flywheel-io/scientific-solutions/gears/gear-exchange"
    git_clone "${GEAR_EXCHANGE_REPO}"
    test -d "/tmp/$GEAR_EXCHANGE_REPO" || die "not found: /tmp/$GEAR_EXCHANGE_REPO directory"
    create_or_update_gear_json
    git_mr "update-${GEAR_NAME}-version" "$CI_COMMIT_BRANCH" \
            title="${GEAR_IMG}" description="Update ${GEAR_NAME} version to ${CURRENT_GEAR_VERSION}"

}


create_or_update_gear_json(){
  log "Creating or updating gear json..."
#  cd "/tmp/$GEAR_EXCHANGE_REPO"
  GEARS_DIR="/tmp/${GEAR_EXCHANGE_REPO}/gears/${IMG_NAMESAPCE}"
  # Check the manifest.json
  if [ -f "${GEARS_DIR}/${GEAR_NAME}.json" ]; then
      log "${GEAR_NAME}.json exists."
      log "Validating gear version"
      # TODO check if the version is the same with current manifest.json
      GEAR_EXCHANGE_GEAR_VER=$(jq -r .version "${GEARS_DIR}/${GEAR_NAME}.json" | sed 's/_.*//')
      [[ "$CURRENT_GEAR_VERSION" = "$GEAR_EXCHANGE_GEAR_VER" ]] && die "found: $CURRENT_GEAR_VERSION version in gear repo, expecting to be different from gear-exchange version - $GEAR_EXCHANGE_GEAR_VER."
  else
      # If the file does not exist, copy manifest.json to the target directory with a new name
      log "File does not exist on gear-exchange repo."
  fi
  cp ./manifest.json "${GEARS_DIR}"/"${GEAR_NAME}".json
  # Then call git_mr to do the merge request
}
