# SSE QA-CI - Gitlab CI templates inherited from QA-CI

This repository will be hosting SSE's specific Gitlab CI templates
that is inherited from [QA-CI](https://gitlab.com/flywheel-io/tools/etc/qa-ci).

**Description**:

Standardized development workflows and continuous integration pipelines using

- [pre-commit](https://pre-commit.com/) for running linters and tests locally
  with `pre-commit run -a`
- [GitLab CI](https://docs.gitlab.com/ee/ci/) jobs for doing the same in CI
  then publishing artifacts and running further automation.

## Table of Contents

[TOC]

## Usage

- Ensure you will need to have `bash`, `docker` and `pre-commit` installed

```bash
brew install bash docker pre-commit
```

- Create/Update `.gitlab-ci.yml` file in your project

<!-- markdownlint-disable MD013 -->
_Checkout the
[skeleton template](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/skeleton/-/blob/main/.gitlab-ci.yml) for the latest ref to use in Gitlab CI._

```yaml
include:
  - project: flywheel-io/scientific-solutions/etc/sse-qa-ci
    ref: db9f40c2155f22ec6d1beccddc3be0126fd4299f # latest ref on 04102023
    file: ci/default.yml

variables:
  PYTEST_COV_FAIL_UNDER: 80 # variables for pytest coverage. Recommend to set to 80
  DEBUG: "3"  # Set debug mode to print out executed command on qa-ci. "3" will have detailed information and "9" will provide more robust message
  PUBLISH_POETRY: "true"  # Set to true to allow CI to publish package to PYPI
  CACHE_CLEAR: "1"  # Set to 1 to clear runner cache on gitlab CI. Suggest having this set if you switch to the new qa-ci

```

- Create/Update `.pre-commit-config.yaml`
file in your project

_Checkout the
[skeleton template](https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/skeleton/-/blob/main/.pre-commit-config.yaml) for the latest recommended pre-commit hooks that are available._

```yaml
repos:
  - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
    rev: 8d37c3b4565f992dabbb3c95fb63fd80e155a051 # latest ref on 04102023
    hooks:
      - id: gearcheck
        always_run: true
      - id: poetry_export
      - id: docker_build
      - id: markdownlint
      - id: yamllint
      - id: black
      - id: isort
      - id: pytest

```

_Notes:_ The pre-commit hooks above is what
we recommend to use on Flywheel Gears repository.
Feel free to add additional pre-commit hooks that
is available based on your use case. See [Supported Hooks](#supported-hooks)

## pre-commit

### Hook configuration

_The following was captured from the qa-ci documentation on [Hook Configuration](https://gitlab.com/flywheel-io/tools/etc/qa-ci#hook-configuration)_

- Hooks are configured with `.pre-commit-config.yaml`.<br/>
  Projects using the py/docker stack should use the **recommended hooks**
  listed in the [Usage](#usage) section above and documented in detail in the
  [Supported hooks](#supported-hooks) section below.

#### Custom Args

- Hooks come with a **recommended configuration** to aid in code standardization
  across projects. You can override the defaults by passing your own hook args:

    ```yaml
    hooks:
      - id: pydocstyle
        args: [--convention=numpy]  # override qa-ci's default [--convention=google]
    ```

#### View Hooks/CLI option

- You can inspect a CLI tools' help text to discover their available args:

    ```bash
    docker run -it --rm flywheel/qa-ci pydocstyle --help
    ```

#### Configuring in `pyproject.toml`

- You may prefer to centralize python-related hook configuration in `pyproject.toml`
  for tools that support it like isort, mypy, pydocstyle, pylint and pytest.<br/>
  Override the default hook args with `args: []` to allow the `pyproject.toml`
  settings to take precedence if needed:

    ```yaml
    hooks:
      - id: pylint
        args: []  # unset default args to allow the config file to take effect
    ```

#### Disabling hooks

- Hooks **can be disabled** by commenting them out, which is useful for adopting
  them gradually in existing projects:

    ```yaml
    hooks:
    # - id: mypy  # disable mypy type check by commenting it out
    ```

#### Extending hooks

- Hooks **can be extended** with [3rd party hooks](https://pre-commit.com/hooks.html)
  and custom [local](https://pre-commit.com/#repository-local-hooks) entries:

    ```yaml
    # add 3rd party hook after the qa-ci ones:
    - repo: https://github.com/pre-commit/pre-commit-hooks
      rev: v4.3.0
      hooks:
        - id: trailing-whitespace
    ```

#### Removing hooks

- Finally, hooks **can be removed** if they are not relevant to your project.
  For example:

  - projects that don't have a Dockerfile have no use for `docker_build`
  - python projects where type hints are out of scope could remove the `mypy` hook

### Supported hooks

#### gearcheck

This hook will be validating the following items based on the values found in `manifest.json`:

- Validate the module name in `pyproject.toml`
file is following the expected format:</br> `fw-gear-<GEAR_NAME_FROM_MANIFEST>`
- Validate the module version in `pyproject.toml`
file is aligned with the version specified in `manifest.json`
- Validate the Python package directory
exists in the project and
is named `fw_gear_<GEAR_NAME_FROM_MANIFEST>`

Furthermore, it will validate
the `manifest.json` file in your project.
It will also validate
your docker in your local machine
is running/installed.
This hook will ensure that `.dockerignore`
file exists in your project and that the file
starts with black-listing everything using `**`
and white-lists the
package directory (`fw_gear_<GEAR_NAME_FROM_MANIFEST>`).

#### poetry_export

Export python dependencies from `pyproject.toml` to `requirements.txt` with poetry.

Python project dependencies should be managed with poetry.
Exporting the required packages with their versions pinned into the simpler
requirements.txt format allows installing them pip:

```bash
pip install -r requirements.txt
```

This can be useful for writing a `Dockerfile` without installing poetry for example,
since it's a large, hard-to-containerize utility. When using `poetry_export` for
this purpose, it should run _before_ the `docker_build` hook so that the build
can use the latest `requirements.txt`.

- Prod dependencies will be exported into `requirements.txt`.
- Dev-only dependencies will be exported into `requirements-dev.txt`.
- Extras named `all` will also be included in `requirements-dev.txt`.
- If there are _any_ extras, `all` is required for this hook to work.

Links:

- [poetry](https://github.com/python-poetry/poetry)
- [scripts/poetry_export.sh](scripts/poetry_export.sh)

#### docker_build

Build the project's image from the `Dockerfile` with `docker build`. This hook
should be enabled if you have a `Dockerfile` to verify that it can be built.<br/>
In addition, it will be used in qa-ci hooks that are run within your image context
such as mypy, pydocstyle, pylint and pytest.

Links:

- [poetry](https://github.com/python-poetry/poetry)

#### markdownlint

Lint markdown files for syntax, line length, style and more with markdownlint.
Re-formats `.md` files in-place to consistently use indentation and newlines.

- The CLI arguments (hook args) can be used to enable/disable linter rules
- To access all config options, you need to use a config file named `.markdownlint.yaml`
- On projects without a config, defaults to the qa-ci config file that sets the max
  line length to `88` and allow inline HTML like `<br/>`.

Links:

- [markdownlint](https://github.com/DavidAnson/markdownlint)
- [.markdownlint.yaml (qa-ci)](.markdownlint.yaml)
- [.markdownlint.yaml (sample)](https://github.com/DavidAnson/markdownlint/blob/main/schema/.markdownlint.yaml)
- [Markdown cheatsheet](https://devhints.io/markdown)
- [Markdown editor](https://stackedit.io/app#)
- [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Mermaid cheatsheet](https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/)
- [Mermaid editor](https://mermaid-js.github.io/mermaid-live-editor/edit)

#### linkcheck

Check text files for dead links with a custom qa-ci script `linkcheck.py`.

Looks for HTTP links in any text file using regex and attempts to issue a HEAD
request on all the URLs found. Ignores links

- without a top level domain (eg. `localhost`)
- with top level domain `local` and `test`
- with domain `local.flywheel.io`
- matching any of the `--ignore` patterns passed as arguments

When checking links, strip URL fragments and consider:

- `2xx`, `401` and `403` as success (assume unauthorized responses are OK)
- `503` as success for gitlab links (which return 503 on private repos)
- everything else as failure

In addition to HTTP URLs, check markdown header / file references.

Links:

- [scripts/linkcheck.py](scripts/linkcheck.py)
- [HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

#### jsonlint

Lint JSON files for syntax, uniform indentation and style with jsonlint.
Formats `.json` files in-place to consistently use 2 spaces for indentation.

Links:

- [jsonlint](https://github.com/kevcenteno/jsonlint)

#### yamllint

Lint YAML files for syntax, uniform indentation and style with yamllint. Formats
`.yaml` files in-place to consistently use 2 spaces as indentation and to enforce
maximum line length `88` - among many other rules.

Links:

- [yamllint](https://github.com/adrienverge/yamllint)
- [YAML cheatsheet](https://quickref.me/yaml)
- [YAML multiline strings](https://yaml-multiline.info/)

#### shellcheck

Lint shell scripts with the static analyzer shellcheck.

Links:

- [shellcheck](https://github.com/koalaman/shellcheck)
- [Bash best practices](https://kvz.io/bash-best-practices.html)
- [Bash cheatsheet](https://quickref.me/bash)
- [Shell parameter expansion](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html)

#### hadolint

Lint `Dockerfile` to enforce best practices with hadolint. Uses shellcheck under
the hood to lint the `RUN` instructions as well.

Links:

- [hadolint](https://github.com/hadolint/hadolint)
- [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

#### black

Format python code with black to ensures uniform whitespace and style.

Links:

- [black](https://github.com/psf/black)

#### isort

Sort python import statements according to PEP8 with isort.

Links:

- [isort](https://github.com/pycqa/isort/)
- [PEP8 - Imports](https://peps.python.org/pep-0008/#imports)

#### mypy

Check python type annotations with mypy.

Links:

- [mypy](https://github.com/python/mypy)
- [python - typing](https://docs.python.org/3/library/typing.html)

#### pydocstyle

Check python docstring convention compliance with pydocstyle.

Links:

- [pydocstyle](https://github.com/PyCQA/pydocstyle)
- [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)
- [Example Google Style Docstring](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)

#### pylint

Lint python code with static analyzer pylint.

Links:

- [pylint](https://github.com/PyCQA/pylint)

#### pylint_tests

Lint python tests using less strict defaults with static analyzer pylint.

#### pytest

Run python tests and report code coverage with pytest.

Links:

- [pytest](https://github.com/pytest-dev/pytest/)

### Recommended pre-commit hooks for SSE use case

In this section, you can find a
few `.pre-commit-config.yaml` file template
for different use case.

#### Bare minimum template

This template will mainly check for linting for Python and YAML file,
gear naming/version validation,
docker build and pytest.

```yaml
repos:
  - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
    # TODO check for latest ref to use on skeleton OR poetry-cow-says repo.
    rev: be988923b066bb3ed65adf138103fa7f881a0922 
    hooks:
      - id: gearcheck
        always_run: true
      - id: poetry_export
      - id: docker_build
      - id: yamllint
      - id: black
      - id: isort
      - id: pytest
```

#### Strict template

This template is an extended version of the bare minimum template
with a few linting options for other file types,
such as markdown, dockerfile etc.

```yaml
repos:
  - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
    # TODO check for latest ref to use on skeleton OR poetry-cow-says repo.
    rev: be988923b066bb3ed65adf138103fa7f881a0922 
    hooks:
      - id: gearcheck
        always_run: true
      - id: poetry_export
      - id: docker_build 
      - id: markdownlint
      - id: jsonlint
      - id: yamllint
      - id: hadolint
      - id: black
      - id: isort
      - id: pytest
```

#### Python focused template

This template is an extended version of the bare minimum template
with a few extra linting options for Python docstring/type checker.

```yaml
repos:
  - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
    # TODO check for latest ref to use on skeleton OR poetry-cow-says repo.
    rev: be988923b066bb3ed65adf138103fa7f881a0922 
    hooks:
      - id: gearcheck
        always_run: true
      - id: poetry_export
      - id: docker_build 
      - id: yamllint
      - id: black
      - id: isort
      - id: mypy
      - id: pydocstyle
      - id: pylint
      - id: pytest
```

#### All in one template

This template will include every
pre-commit hooks that are in the
Advanced and Python focused template.

```yaml
repos:
  - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
    # TODO check for latest ref to use on skeleton OR poetry-cow-says repo.
    rev: be988923b066bb3ed65adf138103fa7f881a0922 
    hooks:
      - id: gearcheck
        always_run: true
      - id: poetry_export
      - id: docker_build 
      - id: markdownlint
      - id: linkcheck
      - id: jsonlint
      - id: yamllint
      - id: hadolint
      - id: black
      - id: isort
      - id: mypy
      - id: pydocstyle
      - id: pylint
      - id: pytest
```

## Pipelines

### MR Pipeline

This pipeline happens when MR is created on the repository.
It will trigger `test.pre-commit` and `publish:docker` CI job.

- `test.pre-commit` job
  - Run the pre-commit hooks defined in the project's `.pre-commit-config.yaml` file.
- `publish:docker` job
  - Build, tag and publish docker images from the project to Gitlab Container Registry (default)
  or Dockerhub (need to configured via
  exporting `DOCKER_IMAGE` variable on `.gitlab-ci.yml` file)

## Release Pipeline

This pipeline can be triggered manually on the
project Pipeline page (eg: <<https://gitlab.com/flywheel-io/scientific-solutions/gears/><project-name>/-/pipelines>)
by specify `RELEASE` variable with the version of the release.

 <!--- add a snapshot on what it looks like on the UI -->

Once this pipeline got triggered, `release:mr` CI job will kick off.
This job will create an MR with
the following title `Release <version-you-specify>`.
In the MR created by `release:mr` CI job, you will
find the gear version on `manifest.json` and `pyproject.toml`
files updated to the version that you have specified earlier.
In order to move forward on this pipeline,
you will need to review the changes made by the CI job and approve + merge the MR.

Once you have merged the MR above, a set of CI jobs will get triggered.
This includes the following CI jobs:

- `test:pre-commit`
- `publish:docker`
  - _Notes_: The docker image will be pushed to CI registry instead of Dockerhub
- `release:tag`
  - Create an annotated tag on `main`/`master` branch.
  The version is read from the `VERSION`
variable that used to trigger the `Release Pipeline` .
The new tag in turn should kick off new set of jobs:
    - `publish:poetry`
      - Publish public python projects using poetry to PYPI, the python package index.
      - Only runs on tags and if `PUBLISH_POETRY` is explicitly enabled.
      - Configuration via `.gitlab-ci.yml` / `variables`:
        - `PUBLISH_POETRY`: set to `"true"` to enable the job (default: `"false"`)
    - `publish:docker`
      - _Notes_: When the repository got tagged, the docker image will be published to Dockerhub
    - `update:release`
      - Create a GitLab [project release](https://docs.gitlab.com/ee/user/project/releases/)
      with a changelog and JIRA ticket list gathered from the MRs merged since the last tag.
